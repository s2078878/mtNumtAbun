# mtNumtAbun - Relative NUMT Abundance Calculation

This Nextflow pipeline will calculate the realtive abundance of NUMTs within normal and tumour samples.

## Running the Pipeline

### 1.Cloning Repository

Clone the repository and cd into the directory using the following commands:

```shell
git clone https://git.ecdf.ed.ac.uk/s2078878/mtNumtAbun.git
cd mtNumtAbun
```

### 2.Create Environment

To create a conda environment with all the necessary packages, run the following command:

```shell
conda env create --file env.yml
```

Then activate the environment
```shell
conda activate mtNumtAbun
```

### 3.Make Support Files

The Nextflow pipeline works from a .csv file containing the main parameters needed. The csv file should have the following format and column names:

| row | sampleID | group  | bam                          | index                        |
|:----|:---------|:-------|:-----------------------------|:-----------------------------|
| 1   | sample1  | normal | path/to/sample/alignment.bam | path/to/sample/index.bam.bai |
| 2   | sample1  | tumour | path/to/sample/alignment.bam | path/to/sample/index.bam.bai |
| 3   | sample2  | normal | path/to/sample/alignment.bam | path/to/sample/index.bam.bai |
| 4   | sample2  | tumour | path/to/sample/alignment.bam | path/to/sample/index.bam.bai |

Alignment and index files can be for the entire genome, there is no need to subset to only the mitochondrial DNA.

### 4.Run mtNumtAbun

Everything should now be ready to run mtNumtAbun. A typical command will look something similar to the one below, substituting in the path to your own parameter file. 

```shell
nextflow run mtNumtAbun.nf \
  --parameters path/to/parameters/file.csv \ # parameters file (see step 4)
  --outdir path/to/out/directory/ # where should the results folder be created?
```

This will output a results directory containing NUMT abundance predictions for normal and tumour samples. 

