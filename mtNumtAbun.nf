#!/usr/bin/env nextflow

log.info """\
         =============================================
         N U M T   A B U N D A N C E   P I P E L I N E
         =============================================
         parameters_file:   ${params.parameters}
         out_directory:     ${params.outdir}
         =============================================
         """
         .stripIndent()

/*
 * Input parameters validation
 */
if (params.parameters){
    
    parameters_file = file(params.parameters)

    Channel
        .fromPath(params.parameters)
        .splitCsv(header:true)
        .map{ row -> tuple(row.row, row.sampleID, row.group, row.bam, row.index)}
        .set{ parameters_ch }
    
    if( !parameters_file.exists() ) exit 1, "ERROR: Parameters file doesn't exist: ${parameters_file}"

} else {
    
    exit 1, "ERROR: Missing parameters file. Please set a parameters file using the --parameters flag."

}

/*
 * Find all read IDs where read pairs are mapped between the nuclear and mitochondrial DNA
 */
process ids {
    tag "Getting nuclear-mtdna read pair IDs"

    input:
    set row, sampleID, group, path(bam), path(index) from parameters_ch

    output:
    set sampleID, group, path(bam), path("${sampleID}.IDs.txt") into mitochondria_ch

    script:
    """
    samtools view ${bam} chrM | cut -f1 > ${sampleID}.IDs.txt
    """
}

/*
 * Extract all mitochondria-nuclear read pairs and put into new sam file
 */
process reads {
    tag "Subsetting bam file for nDNA-mtDNA reads"

    input:
    set sampleID, group, path(bam), path(ids) from mitochondria_ch

    output:
    set sampleID, group, path(bam), path("${sampleID}.NUMT.bam") into numt_reads_ch

    script:
    """
    picard FilterSamReads -I ${bam} \
        -O ${sampleID}.NUMT.bam \
        -RLF ${ids} \
        --FILTER includeReadList \
        --QUIET \
        --VALIDATION_STRINGENCY SILENT \
        --TMP_DIR ./
    """
}

/*
 * Count the reads across the genome
 */
process count {
    tag "Counting mapped reads in windows"

    input:
    set sampleID, group, path(bam), path(numts) from numt_reads_ch

    output:
    set path("${sampleID}.${group}.WG.txt"), path("${sampleID}.${group}.MT.txt") into counts_ch

    script:
    """
    cp $baseDir/data/other/windows.txt .
    mkdir genome
    samtools view -H ${bam} | grep -P "@SQ\tSN:" | sed 's/@SQ\tSN://' | sed 's/\tLN:/\t/' > genome/genome.txt
    bedtools coverage -sorted -counts -g genome/genome.txt -a windows.txt -b ${bam} > ${sampleID}.${group}.WG.txt
    bedtools coverage -counts -g genome/genome.txt -a windows.txt -b ${numts} > ${sampleID}.${group}.MT.txt
    """
}

/*
 * Merge all of the counts across the mt and n genomes 
 */
process merge {
    tag "Merging all read counts"

    input:
    path(counts) from counts_ch.collect()

    output:
    path("mergedCounts.txt.gz") into merged_ch

    script:
    """
    cp $baseDir/scripts/mergeCounts.R .
    Rscript mergeCounts.R ${counts}
    """
}

/*
 * Calculate the NUMT abundance
 */
process calculate {
    tag "Calculating NUMT abundance"

    input:
    path(merged) from merged_ch

    output:
    path("numtAbundance.txt")

    script:
    """
    cp $baseDir/scripts/calculateAbundance.R .
    Rscript calculateAbundance.R ${merged}
    """
}

workflow.onComplete {
	log.info ( workflow.success ? "\nDone! NUMT abundance calculated.\n" : "\nOops .. something went wrong.\n" )
}